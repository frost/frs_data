# Dataset Description

This dataset is a large-scale set of measurements for RSS-based localization.
The data consists of received signal strength (RSS) measurements taken using 
the POWDER Testbed at the University of Utah. Samples include either 0, 1, or 2
active transmitters.

The dataset consists of 5,214 unique samples, with transmitters in 5,514 unique
locations. The majority of the samples contain only 1 transmitter, but there are
small sets of samples with 0 or 2 active transmitters, as shown below.
Each sample has RSS values from between 10 and 25 receivers. The majority of the
receivers are stationary endpoints fixed on the side of buildings, on rooftop
towers, or on free-standing poles. A small set of receivers are located on 
shuttles which travel specific routes throughout campus.

| Dataset Description | Sample Count | Receiver Count |
| ------------------- | ----         | -------------- |
| No-Tx Samples       | 46           | 10 to 25       |
| 1-Tx Samples        | 4822         | 10 to 25       |
| 2-Tx Samples        | 346          | 11 to 12       |

The transmitters for this dataset are handheld walkie-talkies transmitting
in the FRS/GMRS band at 462.7 MHz. These devices have a rated transmission power
of 1 W.  The raw IQ samples were processed through a 6 kHz bandpass filter to
remove neighboring transmissions, and the RSS value was calculated as follows:
$$ RSS = \frac{10}{N} \log_{10}\left(\sum_i^N x_i^2   \right) $$

| Measurement Parameters | Description |
| -----------            | ----------- |
| Frequency              | 462.7 MHz   |
| Radio Gain             | 35 dB       |
| Receiver Sample Rate   | 2 MHz       |
| Sample Length          | $N=10,000$  |
| Band-pass Filter       | 6 kHz       |
| Transmitters           | 0 to 2      |
| Transmission Power     | 1 W         |

Receivers consist of Ettus USRP X310 and B210 radios, and a mix of wide- and 
narrow-band antennas, as shown in the table below
Each receiver took measurements with a receiver gain of 35 dB. However, devices
have different maxmimum gain settings, and no calibration data was available, so
all RSS values in the dataset are uncalibrated, and are only relative to the device.

# Instructions for Importing Data
Data is provided in `.json`  format, both as one file and as split files.

## Usage Instructions 
```
import json
data_file = 'powder_462.7_rss_data.json'
with open(data_file) as f:
    data = json.load(f)
```
The `json` data is a dictionary with the sample timestamp as a key. Within each sample are the following keys:
* `rx_data`: A list of data from each receiver. Each entry contains RSS value, latitude, longitude, and device name.
* `tx_coords`: A list of coordinates for each transmitter. Each entry contains latitude and longitude.
* `metadata`: A list of dictionaries containing metadata for each transmitter, in the same order as the rows in `tx_coords`

## File Separations and Train/Test Splits
In the `sorted_data` folder there are several train/test separations of the data. 
* `all_data` contains all the data in the main JSON file, separated by the number of transmitters.
* `stationary` consists of 13 cases where a stationary receiver remained in one location for several minutes, or moved in a small local area. This may be useful for evaluating localization, or measuring the variation in the channel characteristics for stationary receivers. The initial 3 cases have a fixed location, but `stationary4-13` have several radio locations, denoted in the `circle` metadata file:
  * `circle == 0` means the radio was completely stationary
  * `circle == 1` means the radio was held in place, but rotated in the horizontal plane, centered on the antenna. The antenna remained vertical during rotation.
  * `circle == 32` means the radio rotated around a central point in a circle with a radius of 0.32 m (1/2 wavelength). The radio was rotated around the circle, so the orientation of the antenna changed as it rotated around the circle. 
  * `circle == 63` means the radio rotated with a circle of 0.63 m (1 wavelength)
  * `circle == 126` means the radio rotated with a circle of 1.26 m (2 wavelength)
* `train_test_splits` contains unique data splits used for training and evaluating ML models.  These splits only used data from the single-tx case. In other words, the union of each splits, along with `unused.json`, is equivalent to the file `all_data/single_tx.json`. 
  * The `random` split is a random 80/20 split of the data from April and July
  * `special_test_cases` contains the stationary transmitter data, indoor transmitter data (with high noise in GPS location), and transmitters off campus. 
  * The `grid` split divides the campus region in to a 10 by 10 grid.
    Each grid square is assigned to the training or test set, with 80 
    squares in the training set and the remainder in the test set. If 
    a square is assigned to the test set, none of its four neighbors are 
    included in the test set. Transmitters occuring in each grid square 
    are assigned to train or test. One such random assignment of grid squares makes up the `grid` split. (April and July data only)
  * The `seasonal` split contains data separated by the month of collection, in April or July, or November.
  * The `transportation` split contains data separated by the method of movement for the transmitter: walking, cycling, or driving. The `non-driving.json` file contains the union of the walking and cycling data. Data collected later in November is included in separate files.
  * `campus.json` contains the on-campus data from April and July, so is equivalent to the union of each split, not including `unused.json`.

# Digital Surface Model
The dataset includes a digital surface model (DSM) from a State of Utah 2013-2014 LiDAR [survey](https://doi.org/10.5069/G9TH8JNQ). This
map includes the University of Utah campus and surrounding area.
The DSM includes buildings and trees, unlike some digital elevation models.

To read the data in python:
```
import rasterio as rio
import numpy as np
import utm

dsm_object = rio.open('dsm.tif')
dsm_map = dsm_object.read(1)     # a np.array containing elevation values
dsm_resolution = dsm_object.res     # a tuple containing x,y resolution (0.5 meters) 
dsm_transform = dsm_object.transform     # an Affine transform for conversion to UTM-12 coordinates
utm_transform = np.array(dsm_transform).reshape((3,3))[:2]
utm_top_left = utm_transform @ np.array([0,0,1])
utm_bottom_right = utm_transform @ np.array([dsm_object.shape[0], dsm_object.shape[1], 1])
latlon_top_left = utm.to_latlon(utm_top_left[0], utm_top_left[1], 12, 'T')
latlon_bottom_right = utm.to_latlon(utm_bottom_right[0], utm_bottom_right[1], 12, 'T')

```
**Dataset Acknowledgement:** This DSM file is acquired by the State of Utah and its partners, and is in the public domain and can be freely distributed with proper credit to the State of Utah and its partners.  The State of Utah and its partners makes no warranty, expressed or implied, regarding its suitability for a particular use and shall not be liable under any circumstances for any direct, indirect, special, incidental, or consequential damages with respect to users of this product. 

**DSM DOI:** https://doi.org/10.5069/G9TH8JNQ
